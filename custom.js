var sliderCode1;
var sliderCode2;
var carouselSettings1={"pager":false,minSlides:3,maxSlides:3,slideWidth:298,sliderMargin:12,moveSlides: 1}
var carouselSettings2 = {"pager":false};
jQuery(document).ready(function(){	
	jQuery('.review-ratings').append('<a href="#tabReviews" title="tabReviews"></a>');
	
	jQuery('a[href="#tabReviews"]').on('click',function(){
		jQuery('a[href="#reviews"]').trigger('click');
	})
	
	if(jQuery('body').hasClass('single-products')){
		var htmlContent = jQuery('#product-benefit-points').clone();
		jQuery('.product-detail').find('#product-benefit-points').remove();
		jQuery('.product-detail').after(htmlContent);
	}
	jQuery('.how-to-use > a').on('click',function(){
		jQuery('#how-to-use').show();
		setTimeout(function(){
			$(this).trigger('click')
		},300);
	});
	jQuery('#how-to-use h2').on('click',function(){
		jQuery('html,body').animate({scrollTop:(jQuery('.how-to-use').offset().top-jQuery('#header').height())},'slow');
		jQuery('#how-to-use').hide();
	});
	jQuery('.review-ratings > a').on('click',function(){
		jQuery('#reviews').show();
		setTimeout(function(){
			jQuery(this).trigger('click')
		},300);
	});
	jQuery(document.body).on('click','#reviews .bv-action-bar-header',function(){
		jQuery('html,body').animate({scrollTop:(jQuery('.review-ratings').offset().top-jQuery('#header').height())},'slow');
		jQuery('#reviews').hide();
	});
  jQuery('.page-template-about-us a.toggle').click(function(e) {
        e.preventDefault();
       
         var $this = jQuery(this);
       
         if ($this.next(".inner").hasClass('show')) {
             $this.next(".inner").removeClass('show');
             $this.removeClass('active');
             $this.next(".inner").slideUp(350);

         } else {
             $this.parent().parent().find('li .inner').removeClass('show');
             $this.parent().parent().find('li a').removeClass('active');
             $this.parent().parent().find('li .inner').slideUp(350);
             $this.next().toggleClass('show')
             $this.toggleClass('active');
             $this.next().slideToggle(350);
         }
     });
	 
	/*************************Slider code Start*****************************/
	
	 if(jQuery('body').hasClass('home')){
		//Slider Code for homepage Teasers
			
					/*** bxSlider v4.2.12**/
		!function(t){var e={mode:"horizontal",slideSelector:"",infiniteLoop:!0,hideControlOnEnd:!1,speed:500,easing:null,slideMargin:0,startSlide:0,randomStart:!1,captions:!1,ticker:!1,tickerHover:!1,adaptiveHeight:!1,adaptiveHeightSpeed:500,video:!1,useCSS:!0,preloadImages:"visible",responsive:!0,slideZIndex:50,wrapperClass:"bx-wrapper",touchEnabled:!0,swipeThreshold:50,oneToOneTouch:!0,preventDefaultSwipeX:!0,preventDefaultSwipeY:!1,ariaLive:!0,ariaHidden:!0,keyboardEnabled:!1,pager:!0,pagerType:"full",pagerShortSeparator:" / ",pagerSelector:null,buildPager:null,pagerCustom:null,controls:!0,nextText:"Next",prevText:"Prev",nextSelector:null,prevSelector:null,autoControls:!1,startText:"Start",stopText:"Stop",autoControlsCombine:!1,autoControlsSelector:null,auto:!1,pause:4e3,autoStart:!0,autoDirection:"next",stopAutoOnClick:!1,autoHover:!1,autoDelay:0,autoSlideForOnePage:!1,minSlides:1,maxSlides:1,moveSlides:0,slideWidth:0,shrinkItems:!1,onSliderLoad:function(){return!0},onSlideBefore:function(){return!0},onSlideAfter:function(){return!0},onSlideNext:function(){return!0},onSlidePrev:function(){return!0},onSliderResize:function(){return!0}};t.fn.bxSlider=function(n){if(0===this.length)return this;if(this.length>1)return this.each(function(){t(this).bxSlider(n)}),this;var s={},o=this,r=t(window).width(),a=t(window).height();if(!t(o).data("bxSlider")){var l=function(){t(o).data("bxSlider")||(s.settings=t.extend({},e,n),s.settings.slideWidth=parseInt(s.settings.slideWidth),s.children=o.children(s.settings.slideSelector),s.children.length<s.settings.minSlides&&(s.settings.minSlides=s.children.length),s.children.length<s.settings.maxSlides&&(s.settings.maxSlides=s.children.length),s.settings.randomStart&&(s.settings.startSlide=Math.floor(Math.random()*s.children.length)),s.active={index:s.settings.startSlide},s.carousel=s.settings.minSlides>1||s.settings.maxSlides>1,s.carousel&&(s.settings.preloadImages="all"),s.minThreshold=s.settings.minSlides*s.settings.slideWidth+(s.settings.minSlides-1)*s.settings.slideMargin,s.maxThreshold=s.settings.maxSlides*s.settings.slideWidth+(s.settings.maxSlides-1)*s.settings.slideMargin,s.working=!1,s.controls={},s.interval=null,s.animProp="vertical"===s.settings.mode?"top":"left",s.usingCSS=s.settings.useCSS&&"fade"!==s.settings.mode&&function(){for(var t=document.createElement("div"),e=["WebkitPerspective","MozPerspective","OPerspective","msPerspective"],i=0;i<e.length;i++)if(void 0!==t.style[e[i]])return s.cssPrefix=e[i].replace("Perspective","").toLowerCase(),s.animProp="-"+s.cssPrefix+"-transform",!0;return!1}(),"vertical"===s.settings.mode&&(s.settings.maxSlides=s.settings.minSlides),o.data("origStyle",o.attr("style")),o.children(s.settings.slideSelector).each(function(){t(this).data("origStyle",t(this).attr("style"))}),d())},d=function(){var e=s.children.eq(s.settings.startSlide);o.wrap('<div class="'+s.settings.wrapperClass+'"><div class="bx-viewport"></div></div>'),s.viewport=o.parent(),s.settings.ariaLive&&!s.settings.ticker&&s.viewport.attr("aria-live","polite"),s.loader=t('<div class="bx-loading" />'),s.viewport.prepend(s.loader),o.css({width:"horizontal"===s.settings.mode?1e3*s.children.length+215+"%":"auto",position:"relative"}),s.usingCSS&&s.settings.easing?o.css("-"+s.cssPrefix+"-transition-timing-function",s.settings.easing):s.settings.easing||(s.settings.easing="swing"),s.viewport.css({width:"100%",overflow:"hidden",position:"relative"}),s.viewport.parent().css({maxWidth:u()}),s.children.css({float:"horizontal"===s.settings.mode?"left":"none",listStyle:"none",position:"relative"}),s.children.css("width",h()),"horizontal"===s.settings.mode&&s.settings.slideMargin>0&&s.children.css("marginRight",s.settings.slideMargin),"vertical"===s.settings.mode&&s.settings.slideMargin>0&&s.children.css("marginBottom",s.settings.slideMargin),"fade"===s.settings.mode&&(s.children.css({position:"absolute",zIndex:0,display:"none"}),s.children.eq(s.settings.startSlide).css({zIndex:s.settings.slideZIndex,display:"block"})),s.controls.el=t('<div class="bx-controls" />'),s.settings.captions&&P(),s.active.last=s.settings.startSlide===f()-1,s.settings.video&&o.fitVids(),("all"===s.settings.preloadImages||s.settings.ticker)&&(e=s.children),s.settings.ticker?s.settings.pager=!1:(s.settings.controls&&C(),s.settings.auto&&s.settings.autoControls&&T(),s.settings.pager&&w(),(s.settings.controls||s.settings.autoControls||s.settings.pager)&&s.viewport.after(s.controls.el)),c(e,g)},c=function(e,i){var n=e.find('img:not([src=""]), iframe').length,s=0;return 0===n?void i():void e.find('img:not([src=""]), iframe').each(function(){t(this).one("load error",function(){++s===n&&i()}).each(function(){this.complete&&t(this).trigger("load")})})},g=function(){if(s.settings.infiniteLoop&&"fade"!==s.settings.mode&&!s.settings.ticker){var e="vertical"===s.settings.mode?s.settings.minSlides:s.settings.maxSlides,i=s.children.slice(0,e).clone(!0).addClass("bx-clone"),n=s.children.slice(-e).clone(!0).addClass("bx-clone");s.settings.ariaHidden&&(i.attr("aria-hidden",!0),n.attr("aria-hidden",!0)),o.append(i).prepend(n)}s.loader.remove(),m(),"vertical"===s.settings.mode&&(s.settings.adaptiveHeight=!0),s.viewport.height(p()),o.redrawSlider(),s.settings.onSliderLoad.call(o,s.active.index),s.initialized=!0,s.settings.responsive&&t(window).bind("resize",Z),s.settings.auto&&s.settings.autoStart&&(f()>1||s.settings.autoSlideForOnePage)&&H(),s.settings.ticker&&W(),s.settings.pager&&I(s.settings.startSlide),s.settings.controls&&D(),s.settings.touchEnabled&&!s.settings.ticker&&N(),s.settings.keyboardEnabled&&!s.settings.ticker&&t(document).keydown(F)},p=function(){var e=0,n=t();if("vertical"===s.settings.mode||s.settings.adaptiveHeight)if(s.carousel){var o=1===s.settings.moveSlides?s.active.index:s.active.index*x();for(n=s.children.eq(o),i=1;i<=s.settings.maxSlides-1;i++)n=o+i>=s.children.length?n.add(s.children.eq(i-1)):n.add(s.children.eq(o+i))}else n=s.children.eq(s.active.index);else n=s.children;return"vertical"===s.settings.mode?(n.each(function(i){e+=t(this).outerHeight()}),s.settings.slideMargin>0&&(e+=s.settings.slideMargin*(s.settings.minSlides-1))):e=Math.max.apply(Math,n.map(function(){return t(this).outerHeight(!1)}).get()),"border-box"===s.viewport.css("box-sizing")?e+=parseFloat(s.viewport.css("padding-top"))+parseFloat(s.viewport.css("padding-bottom"))+parseFloat(s.viewport.css("border-top-width"))+parseFloat(s.viewport.css("border-bottom-width")):"padding-box"===s.viewport.css("box-sizing")&&(e+=parseFloat(s.viewport.css("padding-top"))+parseFloat(s.viewport.css("padding-bottom"))),e},u=function(){var t="100%";return s.settings.slideWidth>0&&(t="horizontal"===s.settings.mode?s.settings.maxSlides*s.settings.slideWidth+(s.settings.maxSlides-1)*s.settings.slideMargin:s.settings.slideWidth),t},h=function(){var t=s.settings.slideWidth,e=s.viewport.width();if(0===s.settings.slideWidth||s.settings.slideWidth>e&&!s.carousel||"vertical"===s.settings.mode)t=e;else if(s.settings.maxSlides>1&&"horizontal"===s.settings.mode){if(e>s.maxThreshold)return t;e<s.minThreshold?t=(e-s.settings.slideMargin*(s.settings.minSlides-1))/s.settings.minSlides:s.settings.shrinkItems&&(t=Math.floor((e+s.settings.slideMargin)/Math.ceil((e+s.settings.slideMargin)/(t+s.settings.slideMargin))-s.settings.slideMargin))}return t},v=function(){var t=1,e=null;return"horizontal"===s.settings.mode&&s.settings.slideWidth>0?s.viewport.width()<s.minThreshold?t=s.settings.minSlides:s.viewport.width()>s.maxThreshold?t=s.settings.maxSlides:(e=s.children.first().width()+s.settings.slideMargin,t=Math.floor((s.viewport.width()+s.settings.slideMargin)/e)):"vertical"===s.settings.mode&&(t=s.settings.minSlides),t},f=function(){var t=0,e=0,i=0;if(s.settings.moveSlides>0)if(s.settings.infiniteLoop)t=Math.ceil(s.children.length/x());else for(;e<s.children.length;)++t,e=i+v(),i+=s.settings.moveSlides<=v()?s.settings.moveSlides:v();else t=Math.ceil(s.children.length/v());return t},x=function(){return s.settings.moveSlides>0&&s.settings.moveSlides<=v()?s.settings.moveSlides:v()},m=function(){var t,e,i;s.children.length>s.settings.maxSlides&&s.active.last&&!s.settings.infiniteLoop?"horizontal"===s.settings.mode?(e=s.children.last(),t=e.position(),S(-(t.left-(s.viewport.width()-e.outerWidth())),"reset",0)):"vertical"===s.settings.mode&&(i=s.children.length-s.settings.minSlides,t=s.children.eq(i).position(),S(-t.top,"reset",0)):(t=s.children.eq(s.active.index*x()).position(),s.active.index===f()-1&&(s.active.last=!0),void 0!==t&&("horizontal"===s.settings.mode?S(-t.left,"reset",0):"vertical"===s.settings.mode&&S(-t.top,"reset",0)))},S=function(e,i,n,r){var a,l;s.usingCSS?(l="vertical"===s.settings.mode?"translate3d(0, "+e+"px, 0)":"translate3d("+e+"px, 0, 0)",o.css("-"+s.cssPrefix+"-transition-duration",n/1e3+"s"),"slide"===i?(o.css(s.animProp,l),0!==n?o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(e){t(e.target).is(o)&&(o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),q())}):q()):"reset"===i?o.css(s.animProp,l):"ticker"===i&&(o.css("-"+s.cssPrefix+"-transition-timing-function","linear"),o.css(s.animProp,l),0!==n?o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(e){t(e.target).is(o)&&(o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),S(r.resetValue,"reset",0),L())}):(S(r.resetValue,"reset",0),L()))):(a={},a[s.animProp]=e,"slide"===i?o.animate(a,n,s.settings.easing,function(){q()}):"reset"===i?o.css(s.animProp,e):"ticker"===i&&o.animate(a,n,"linear",function(){S(r.resetValue,"reset",0),L()}))},b=function(){for(var e="",i="",n=f(),o=0;o<n;o++)i="",s.settings.buildPager&&t.isFunction(s.settings.buildPager)||s.settings.pagerCustom?(i=s.settings.buildPager(o),s.pagerEl.addClass("bx-custom-pager")):(i=o+1,s.pagerEl.addClass("bx-default-pager")),e+='<div class="bx-pager-item"><a href="" data-slide-index="'+o+'" class="bx-pager-link">'+i+"</a></div>";s.pagerEl.html(e)},w=function(){s.settings.pagerCustom?s.pagerEl=t(s.settings.pagerCustom):(s.pagerEl=t('<div class="bx-pager" />'),s.settings.pagerSelector?t(s.settings.pagerSelector).html(s.pagerEl):s.controls.el.addClass("bx-has-pager").append(s.pagerEl),b()),s.pagerEl.on("click touchend","a",z)},C=function(){s.controls.next=t('<a class="bx-next" href="">'+s.settings.nextText+"</a>"),s.controls.prev=t('<a class="bx-prev" href="">'+s.settings.prevText+"</a>"),s.controls.next.bind("click touchend",E),s.controls.prev.bind("click touchend",k),s.settings.nextSelector&&t(s.settings.nextSelector).append(s.controls.next),s.settings.prevSelector&&t(s.settings.prevSelector).append(s.controls.prev),s.settings.nextSelector||s.settings.prevSelector||(s.controls.directionEl=t('<div class="bx-controls-direction" />'),s.controls.directionEl.append(s.controls.prev).append(s.controls.next),s.controls.el.addClass("bx-has-controls-direction").append(s.controls.directionEl))},T=function(){s.controls.start=t('<div class="bx-controls-auto-item"><a class="bx-start" href="">'+s.settings.startText+"</a></div>"),s.controls.stop=t('<div class="bx-controls-auto-item"><a class="bx-stop" href="">'+s.settings.stopText+"</a></div>"),s.controls.autoEl=t('<div class="bx-controls-auto" />'),s.controls.autoEl.on("click",".bx-start",M),s.controls.autoEl.on("click",".bx-stop",y),s.settings.autoControlsCombine?s.controls.autoEl.append(s.controls.start):s.controls.autoEl.append(s.controls.start).append(s.controls.stop),s.settings.autoControlsSelector?t(s.settings.autoControlsSelector).html(s.controls.autoEl):s.controls.el.addClass("bx-has-controls-auto").append(s.controls.autoEl),A(s.settings.autoStart?"stop":"start")},P=function(){s.children.each(function(e){var i=t(this).find("img:first").attr("title");void 0!==i&&(""+i).length&&t(this).append('<div class="bx-caption"><span>'+i+"</span></div>")})},E=function(t){t.preventDefault(),s.controls.el.hasClass("disabled")||(s.settings.auto&&s.settings.stopAutoOnClick&&o.stopAuto(),o.goToNextSlide())},k=function(t){t.preventDefault(),s.controls.el.hasClass("disabled")||(s.settings.auto&&s.settings.stopAutoOnClick&&o.stopAuto(),o.goToPrevSlide())},M=function(t){o.startAuto(),t.preventDefault()},y=function(t){o.stopAuto(),t.preventDefault()},z=function(e){var i,n;e.preventDefault(),s.controls.el.hasClass("disabled")||(s.settings.auto&&s.settings.stopAutoOnClick&&o.stopAuto(),i=t(e.currentTarget),void 0!==i.attr("data-slide-index")&&(n=parseInt(i.attr("data-slide-index")),n!==s.active.index&&o.goToSlide(n)))},I=function(e){var i=s.children.length;return"short"===s.settings.pagerType?(s.settings.maxSlides>1&&(i=Math.ceil(s.children.length/s.settings.maxSlides)),void s.pagerEl.html(e+1+s.settings.pagerShortSeparator+i)):(s.pagerEl.find("a").removeClass("active"),void s.pagerEl.each(function(i,n){t(n).find("a").eq(e).addClass("active")}))},q=function(){if(s.settings.infiniteLoop){var t="";0===s.active.index?t=s.children.eq(0).position():s.active.index===f()-1&&s.carousel?t=s.children.eq((f()-1)*x()).position():s.active.index===s.children.length-1&&(t=s.children.eq(s.children.length-1).position()),t&&("horizontal"===s.settings.mode?S(-t.left,"reset",0):"vertical"===s.settings.mode&&S(-t.top,"reset",0))}s.working=!1,s.settings.onSlideAfter.call(o,s.children.eq(s.active.index),s.oldIndex,s.active.index)},A=function(t){s.settings.autoControlsCombine?s.controls.autoEl.html(s.controls[t]):(s.controls.autoEl.find("a").removeClass("active"),s.controls.autoEl.find("a:not(.bx-"+t+")").addClass("active"))},D=function(){1===f()?(s.controls.prev.addClass("disabled"),s.controls.next.addClass("disabled")):!s.settings.infiniteLoop&&s.settings.hideControlOnEnd&&(0===s.active.index?(s.controls.prev.addClass("disabled"),s.controls.next.removeClass("disabled")):s.active.index===f()-1?(s.controls.next.addClass("disabled"),s.controls.prev.removeClass("disabled")):(s.controls.prev.removeClass("disabled"),s.controls.next.removeClass("disabled")))},H=function(){if(s.settings.autoDelay>0){setTimeout(o.startAuto,s.settings.autoDelay)}else o.startAuto(),t(window).focus(function(){o.startAuto()}).blur(function(){o.stopAuto()});s.settings.autoHover&&o.hover(function(){s.interval&&(o.stopAuto(!0),s.autoPaused=!0)},function(){s.autoPaused&&(o.startAuto(!0),s.autoPaused=null)})},W=function(){var e,i,n,r,a,l,d,c,g=0;"next"===s.settings.autoDirection?o.append(s.children.clone().addClass("bx-clone")):(o.prepend(s.children.clone().addClass("bx-clone")),e=s.children.first().position(),g="horizontal"===s.settings.mode?-e.left:-e.top),S(g,"reset",0),s.settings.pager=!1,s.settings.controls=!1,s.settings.autoControls=!1,s.settings.tickerHover&&(s.usingCSS?(r="horizontal"===s.settings.mode?4:5,s.viewport.hover(function(){i=o.css("-"+s.cssPrefix+"-transform"),n=parseFloat(i.split(",")[r]),S(n,"reset",0)},function(){c=0,s.children.each(function(e){c+="horizontal"===s.settings.mode?t(this).outerWidth(!0):t(this).outerHeight(!0)}),a=s.settings.speed/c,l="horizontal"===s.settings.mode?"left":"top",d=a*(c-Math.abs(parseInt(n))),L(d)})):s.viewport.hover(function(){o.stop()},function(){c=0,s.children.each(function(e){c+="horizontal"===s.settings.mode?t(this).outerWidth(!0):t(this).outerHeight(!0)}),a=s.settings.speed/c,l="horizontal"===s.settings.mode?"left":"top",d=a*(c-Math.abs(parseInt(o.css(l)))),L(d)})),L()},L=function(t){var e,i,n,r=t?t:s.settings.speed,a={left:0,top:0},l={left:0,top:0};"next"===s.settings.autoDirection?a=o.find(".bx-clone").first().position():l=s.children.first().position(),e="horizontal"===s.settings.mode?-a.left:-a.top,i="horizontal"===s.settings.mode?-l.left:-l.top,n={resetValue:i},S(e,"ticker",r,n)},O=function(e){var i=t(window),n={top:i.scrollTop(),left:i.scrollLeft()},s=e.offset();return n.right=n.left+i.width(),n.bottom=n.top+i.height(),s.right=s.left+e.outerWidth(),s.bottom=s.top+e.outerHeight(),!(n.right<s.left||n.left>s.right||n.bottom<s.top||n.top>s.bottom)},F=function(t){var e=document.activeElement.tagName.toLowerCase(),i="input|textarea",n=new RegExp(e,["i"]),s=n.exec(i);if(null==s&&O(o)){if(39===t.keyCode)return E(t),!1;if(37===t.keyCode)return k(t),!1}},N=function(){s.touch={start:{x:0,y:0},end:{x:0,y:0}},s.viewport.bind("touchstart MSPointerDown pointerdown",X),s.viewport.on("click",".bxslider a",function(t){s.viewport.hasClass("click-disabled")&&(t.preventDefault(),s.viewport.removeClass("click-disabled"))})},X=function(t){if(s.controls.el.addClass("disabled"),s.working)t.preventDefault(),s.controls.el.removeClass("disabled");else{s.touch.originalPos=o.position();var e=t.originalEvent,i="undefined"!=typeof e.changedTouches?e.changedTouches:[e];s.touch.start.x=i[0].pageX,s.touch.start.y=i[0].pageY,s.viewport.get(0).setPointerCapture&&(s.pointerId=e.pointerId,s.viewport.get(0).setPointerCapture(s.pointerId)),s.viewport.bind("touchmove MSPointerMove pointermove",V),s.viewport.bind("touchend MSPointerUp pointerup",R),s.viewport.bind("MSPointerCancel pointercancel",Y)}},Y=function(t){S(s.touch.originalPos.left,"reset",0),s.controls.el.removeClass("disabled"),s.viewport.unbind("MSPointerCancel pointercancel",Y),s.viewport.unbind("touchmove MSPointerMove pointermove",V),s.viewport.unbind("touchend MSPointerUp pointerup",R),s.viewport.get(0).releasePointerCapture&&s.viewport.get(0).releasePointerCapture(s.pointerId)},V=function(t){var e=t.originalEvent,i="undefined"!=typeof e.changedTouches?e.changedTouches:[e],n=Math.abs(i[0].pageX-s.touch.start.x),o=Math.abs(i[0].pageY-s.touch.start.y),r=0,a=0;3*n>o&&s.settings.preventDefaultSwipeX?t.preventDefault():3*o>n&&s.settings.preventDefaultSwipeY&&t.preventDefault(),"fade"!==s.settings.mode&&s.settings.oneToOneTouch&&("horizontal"===s.settings.mode?(a=i[0].pageX-s.touch.start.x,r=s.touch.originalPos.left+a):(a=i[0].pageY-s.touch.start.y,r=s.touch.originalPos.top+a),S(r,"reset",0))},R=function(t){s.viewport.unbind("touchmove MSPointerMove pointermove",V),s.controls.el.removeClass("disabled");var e=t.originalEvent,i="undefined"!=typeof e.changedTouches?e.changedTouches:[e],n=0,r=0;s.touch.end.x=i[0].pageX,s.touch.end.y=i[0].pageY,"fade"===s.settings.mode?(r=Math.abs(s.touch.start.x-s.touch.end.x),r>=s.settings.swipeThreshold&&(s.touch.start.x>s.touch.end.x?o.goToNextSlide():o.goToPrevSlide(),o.stopAuto())):("horizontal"===s.settings.mode?(r=s.touch.end.x-s.touch.start.x,n=s.touch.originalPos.left):(r=s.touch.end.y-s.touch.start.y,n=s.touch.originalPos.top),!s.settings.infiniteLoop&&(0===s.active.index&&r>0||s.active.last&&r<0)?S(n,"reset",200):Math.abs(r)>=s.settings.swipeThreshold?(r<0?o.goToNextSlide():o.goToPrevSlide(),o.stopAuto()):S(n,"reset",200)),s.viewport.unbind("touchend MSPointerUp pointerup",R),s.viewport.get(0).releasePointerCapture&&s.viewport.get(0).releasePointerCapture(s.pointerId)},Z=function(e){if(s.initialized)if(s.working)window.setTimeout(Z,10);else{var i=t(window).width(),n=t(window).height();r===i&&a===n||(r=i,a=n,o.redrawSlider(),s.settings.onSliderResize.call(o,s.active.index))}},B=function(t){var e=v();s.settings.ariaHidden&&!s.settings.ticker&&(s.children.attr("aria-hidden","true"),s.children.slice(t,t+e).attr("aria-hidden","false"))},U=function(t){return t<0?s.settings.infiniteLoop?f()-1:s.active.index:t>=f()?s.settings.infiniteLoop?0:s.active.index:t};return o.goToSlide=function(e,i){var n,r,a,l,d=!0,c=0,g={left:0,top:0},u=null;if(s.oldIndex=s.active.index,s.active.index=U(e),!s.working&&s.active.index!==s.oldIndex){if(s.working=!0,d=s.settings.onSlideBefore.call(o,s.children.eq(s.active.index),s.oldIndex,s.active.index),"undefined"!=typeof d&&!d)return s.active.index=s.oldIndex,void(s.working=!1);"next"===i?s.settings.onSlideNext.call(o,s.children.eq(s.active.index),s.oldIndex,s.active.index)||(d=!1):"prev"===i&&(s.settings.onSlidePrev.call(o,s.children.eq(s.active.index),s.oldIndex,s.active.index)||(d=!1)),s.active.last=s.active.index>=f()-1,(s.settings.pager||s.settings.pagerCustom)&&I(s.active.index),s.settings.controls&&D(),"fade"===s.settings.mode?(s.settings.adaptiveHeight&&s.viewport.height()!==p()&&s.viewport.animate({height:p()},s.settings.adaptiveHeightSpeed),s.children.filter(":visible").fadeOut(s.settings.speed).css({zIndex:0}),s.children.eq(s.active.index).css("zIndex",s.settings.slideZIndex+1).fadeIn(s.settings.speed,function(){t(this).css("zIndex",s.settings.slideZIndex),q()})):(s.settings.adaptiveHeight&&s.viewport.height()!==p()&&s.viewport.animate({height:p()},s.settings.adaptiveHeightSpeed),!s.settings.infiniteLoop&&s.carousel&&s.active.last?"horizontal"===s.settings.mode?(u=s.children.eq(s.children.length-1),g=u.position(),c=s.viewport.width()-u.outerWidth()):(n=s.children.length-s.settings.minSlides,g=s.children.eq(n).position()):s.carousel&&s.active.last&&"prev"===i?(r=1===s.settings.moveSlides?s.settings.maxSlides-x():(f()-1)*x()-(s.children.length-s.settings.maxSlides),u=o.children(".bx-clone").eq(r),g=u.position()):"next"===i&&0===s.active.index?(g=o.find("> .bx-clone").eq(s.settings.maxSlides).position(),s.active.last=!1):e>=0&&(l=e*parseInt(x()),g=s.children.eq(l).position()),"undefined"!=typeof g?(a="horizontal"===s.settings.mode?-(g.left-c):-g.top,S(a,"slide",s.settings.speed)):s.working=!1),s.settings.ariaHidden&&B(s.active.index*x())}},o.goToNextSlide=function(){if(s.settings.infiniteLoop||!s.active.last){var t=parseInt(s.active.index)+1;o.goToSlide(t,"next")}},o.goToPrevSlide=function(){if(s.settings.infiniteLoop||0!==s.active.index){var t=parseInt(s.active.index)-1;o.goToSlide(t,"prev")}},o.startAuto=function(t){s.interval||(s.interval=setInterval(function(){"next"===s.settings.autoDirection?o.goToNextSlide():o.goToPrevSlide()},s.settings.pause),s.settings.autoControls&&t!==!0&&A("stop"))},o.stopAuto=function(t){s.interval&&(clearInterval(s.interval),s.interval=null,s.settings.autoControls&&t!==!0&&A("start"))},o.getCurrentSlide=function(){return s.active.index},o.getCurrentSlideElement=function(){return s.children.eq(s.active.index)},o.getSlideElement=function(t){return s.children.eq(t)},o.getSlideCount=function(){return s.children.length},o.isWorking=function(){return s.working},o.redrawSlider=function(){s.children.add(o.find(".bx-clone")).outerWidth(h()),s.viewport.css("height",p()),s.settings.ticker||m(),s.active.last&&(s.active.index=f()-1),s.active.index>=f()&&(s.active.last=!0),s.settings.pager&&!s.settings.pagerCustom&&(b(),I(s.active.index)),s.settings.ariaHidden&&B(s.active.index*x())},o.destroySlider=function(){s.initialized&&(s.initialized=!1,t(".bx-clone",this).remove(),s.children.each(function(){void 0!==t(this).data("origStyle")?t(this).attr("style",t(this).data("origStyle")):t(this).removeAttr("style")}),void 0!==t(this).data("origStyle")?this.attr("style",t(this).data("origStyle")):t(this).removeAttr("style"),t(this).unwrap().unwrap(),s.controls.el&&s.controls.el.remove(),s.controls.next&&s.controls.next.remove(),s.controls.prev&&s.controls.prev.remove(),s.pagerEl&&s.settings.controls&&!s.settings.pagerCustom&&s.pagerEl.remove(),t(".bx-caption",this).remove(),s.controls.autoEl&&s.controls.autoEl.remove(),clearInterval(s.interval),s.settings.responsive&&t(window).unbind("resize",Z),s.settings.keyboardEnabled&&t(document).unbind("keydown",F),t(this).removeData("bxSlider"))},o.reloadSlider=function(e){void 0!==e&&(n=e),o.destroySlider(),l(),t(o).data("bxSlider",this)},l(),t(o).data("bxSlider",this),this}}}(jQuery);
			jQuery(window).on('load',function(){
					sliderCreation();
			});
			
			jQuery(window).on('resize',function(){
					if(jQuery(document).width()>768){
						if(sliderCode1){
							sliderCode1.destroySlider();
							//sliderCode1.reloadSlider(carouselSettings1);
						}
						if(sliderCode2){
							sliderCode2.destroySlider();
							//sliderCode2.reloadSlider(carouselSettings1);
						}
						
					}
					else{
						if(sliderCode1){
							sliderCode1.reloadSlider(carouselSettings2);
						}
						else{
							sliderCreation();
						}
						if(sliderCode2){
							sliderCode2.reloadSlider();
						}
					}
			});
			
			jQuery('.tabs-container-header li a').on('click',function(){
				if(jQuery(this).parent('li').index()==0){
					if(jQuery(this).hasClass('active')){}
					else{
						jQuery(this).parent('li').siblings().find('a').removeClass('active');
						jQuery(this).addClass('active');
						jQuery('.most-loved-content-wrapper').hide();
						jQuery('.just-in-content-wrapper').show();
						if(jQuery('.just-in-content-wrapper').find('.bx-wrapper').size()>0){
							sliderCode1.reloadSlider();
						}
						else{
							
						}
					}
				}
				else{
					if(jQuery(this).hasClass('active')){}
					else{
						jQuery(this).parent('li').siblings().find('a').removeClass('active');
						jQuery(this).addClass('active');
						jQuery('.just-in-content-wrapper').hide();
						jQuery('.most-loved-content-wrapper').show();
						if(jQuery('.most-loved-content-wrapper').find('.bx-wrapper').size()>0){
							sliderCode2.reloadSlider();
						}
						else{
							
							if(jQuery(window).width()>768){
							if(jQuery('.most-loved-content li').length>3){
									window.sliderCode2 = jQuery('.most-loved-content').bxSlider(carouselSettings1);
								}
							}
							else{
								window.sliderCode2 = jQuery('.most-loved-content').bxSlider(carouselSettings2);
							}
						}
					}
				}
			});
			function sliderCreation(){
				if(jQuery(window).width()>768){
						if(jQuery('.just-in-content li').length>3){
							window.sliderCode1 = jQuery('.just-in-content').bxSlider(carouselSettings1);
							}
					}
					else{
						window.sliderCode1 = jQuery('.just-in-content').bxSlider(carouselSettings2);
					}
			}
			
	}
	/*************************Slider code End*****************************/
});

jQuery( document ).ready(function() {
    /** Video id js**/
    jQuery(".spna_video_id").each(function(){
	var yt_video_id = jQuery(this).attr("id");
	var hidden_video_ele = document.createElement("input");
	hidden_video_ele.setAttribute("type", "hidden");
	hidden_video_ele.setAttribute("class", "video_id");
	hidden_video_ele.setAttribute("value", yt_video_id);
	jQuery( this ).replaceWith(hidden_video_ele);
});
    /**video id js end**/
  var allure_str_home = '<img class="allure_seal_home" src="/wp-content/uploads/sites/2/2016/12/BestofBeauty_2016.png">';
  jQuery(allure_str_home).appendTo('#home div.spotlight-1078 figure');
  
  var allure_str = '<img class="allure_seal_img" src="/wp-content/uploads/sites/2/2016/12/BestofBeauty_2016.png">';
  jQuery('.allure_seal').each(function(){
    jQuery(allure_str).appendTo(this);
  })

  /*Olapic Section start*/
  if(jQuery('body').hasClass('single-products')){

    var protocol = window.location.protocol,
       baseURL = protocol + '//photorankapi-a.akamaihd.net',
       authToken = 'e96e3a4708b9db6f4103b704a49cf5adcd2aca369acc12f6c176b0f1ab7cf3b6',
       apiVersion = 'v2.2',
       customerID = '217523',
       productID = jQuery('script[data-olapic="olapic_specific_widget"]').attr('data-tags'),
       categoryID = '1533_1472639743'; 
       var $ = jQuery;
       $.ajax({
           url: baseURL+'/customers/'+customerID+'/streams/bytag/'+productID+'?auth_token='+authToken+'&version='+apiVersion,
           dataType: 'jsonp',
           type: 'GET',
           success: function(response){
              $.ajax({
                   url: protocol+response.data._embedded['media:recent']._links.self.href+'?auth_token='+authToken+'&version='+apiVersion,
                   dataType: 'JSON',
                   type: 'GET'
                 }) 
                .error(function(error){
                  jQuery(window).load(function(){
                    window.olapic.prepareWidget({'id':'c8b41416da64c144e453b342de342504', 'wrapper':'olapic_main', 'tags' : categoryID}, {'renderNow' : true});                   
                  })

                  }).success(function(result){
                      window.olapic.prepareWidget({'id':'[olapic_product_instance_id]', 'wrapper':'olapic_specific_widget', 'tags' : productID}, {'renderNow' : true});
                  });
                }
      });
  }
  /*Olapic Section end*/

  /*Open search form*/
/********made following change so that this search box opens only on iPad and Above*********/

if(jQuery(window).width()>767){
		jQuery('.search-form-container').hide();
      var str ='<div class="mobile-open-serach">Search</div>';
      jQuery(str).insertAfter('section.banner a.logo');

      jQuery("li.open-search-form").on("click", function(){
        showSearchForm();
      });

      jQuery("div.mobile-open-serach").on("click", function(){
        showSearchForm();
        jQuery(this).css('background-position','0px -29px');
        jQuery('section#main').addClass('searchBoxOpen');
      });

    /*Close search form*/
      jQuery(".close-search-form, .btn-navbar").on("click", function(){
        closeSearchForm();
      });

    /*Clear search text*/
      jQuery(".clear-search-text").on("click", function(){
         clearSearchBox();
      });

     /*Esc key*/ 
      jQuery(document).on('keyup','body',function(e) {
        if (e.keyCode == 27) {
          closeSearchForm();
        }
      });

    /*Clear search text button hide/show*/
      jQuery('.search-form-container .input-medium').keyup(function(){
         if(jQuery('.search-form-container .input-medium').attr('value')){
            jQuery(".clear-search-text").css('display','inline-block');
         }
          else{
            jQuery(".clear-search-text").css('display','none');
          }
      });
	  function closeSearchForm(){
        clearSearchBox();
        jQuery('section#main').removeClass('searchBoxOpen');
        jQuery('#menu-header-navigation').css('display','block');
        jQuery('.search-form-container').css('display','none');  
        jQuery("div.mobile-open-serach").css('background-position','0px 0px'); 
        jQuery('section#main').removeClass('blureffect');
        jQuery('.navbar-static-top .container').addClass('navSearchpaddingbottom');
        jQuery('body').removeClass('bodyNonScroll');
		jQuery('.banner .sign-up').show();
      }

      function clearSearchBox(){
        jQuery('.search-form-container .input-medium').attr('value','');
        jQuery(".clear-search-text").css('display','none');
      }

      function showSearchForm(){
        jQuery('#menu-header-navigation').css('display','none');
        jQuery('.search-form-container').css('display','block');
        jQuery('section#main').addClass('blureffect');
        jQuery('.navbar-static-top .container').removeClass('navSearchpaddingbottom');
        jQuery('body').addClass('bodyNonScroll');
        jQuery(".search-form-container .form-search input[type='text']").focus();
		jQuery('.banner .sign-up').hide();
      }
}
    
/*product and search fixes*/
productsheight();
window.onload = function() {
  productsheight();
};
jQuery('.load_more_text').on('click',function(){
   setTimeout(function(){
  	productsheight();
   },3500);    
});
jQuery(window).on("orientationchange onresize",function(){
  productsheight();
});
function productsheight(){
  jQuery('.search-results .thumbnails li').each(function(i){
        jQuery('.search-results .thumbnails li').css('clear','none');
        });
}
/*Set product height*/
 /*     window.onload = function() {
          if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/)) || (typeof jQuery.browser !== "undefined" && jQuery.browser.msie == 1)){
            setTimeout(function(){
              if(jQuery('body').is('#product')){
                calculateProductsTilesHeight();
              }
              if(jQuery('body').is('#search_result')){
                setsearchproductheight();
              }
            },3000);
          }
          else{
              if(jQuery('body').is('#product')){
                calculateProductsTilesHeight();
              }
              if(jQuery('body').is('#search_result')){
                setsearchproductheight();
              }
          }
      }
      jQuery(window).on("orientationchange onresize",function(){
          setsearchproductheight();
          setTimeout(function(){
            if(jQuery('body').is('#product')){
              calculateProductsTilesHeight();
            }
            if(jQuery('body').is('#search_result')){
              setsearchproductheight();
            }
          },2000);
      });
      jQuery('.load_more_text').on('click',function(){
          setTimeout(function(){
            if(jQuery('body').is('#product')){
              calculateProductsTilesHeight();
            }
            if(jQuery('body').is('#search_result')){
              setsearchproductheight();
            }
          },2000);
      });



      function setsearchproductheight(){
        jQuery('.search-results .thumbnails li').removeAttr('style');
        var array = [],
            largest= 0;
        
        jQuery('.search-results .thumbnails li').each(function(i) {
           jQuery(this).attr('data-originalHeight',''+jQuery(this).height()+'');
            array[i] = jQuery(this).height();
        });
        
        for (var i=0; i<=array.length;i++){
            if (array[i]>largest) {
                largest=array[i];
            }
        }
        jQuery('.search-results .thumbnails li').height(largest+'px');
        jQuery('.search-results .thumbnails li').css('clear','none');
      }


      function calculateProductsTilesHeight(){
       jQuery('#product .product-carousel li').removeAttr('style');
        var array = [],
            largest= 0;
        jQuery('#product .product-carousel li').each(function(i) {
           jQuery(this).attr('data-originalHeight',''+jQuery(this).height()+'');
            array[i] = jQuery(this).height();
        });
        for (var i=0; i<=array.length;i++){
            if (array[i]>largest) {
                largest=array[i];
            }
        }
        jQuery('#product .product-carousel li').height(largest+'px');
      }
*/
      

      /*Place holder*/
      jQuery.support.placeholder = ('placeholder' in document.createElement('input'));
      jQuery(function () {
          if (!jQuery.support.placeholder) {
             jQuery("[placeholder]").focus(function () {
                 if (jQuery(this).val() == jQuery(this).attr("placeholder")) jQuery(this).val("");
             }).blur(function () {
                 if (jQuery(this).val() == "") jQuery(this).val(jQuery(this).attr("placeholder"));
             }).blur();

             jQuery("[placeholder]").parents("form").submit(function () {
                 jQuery(this).find('[placeholder]').each(function() {
                     if (jQuery(this).val() == jQuery(this).attr("placeholder")) {
                         jQuery(this).val("");
                     }
                 });
             });
          }
      });
      
      /* Search form null validation*/
      jQuery('.search-form-container .form-search').submit(function(event) {
        var searchVal = jQuery('.search-form-container .input-medium').val();
        if(!searchVal){
          return false;
        }
      });

});
(function ($) { 
/** Related Products mobile carousle**/
  
  if(window.innerWidth<767){
  $(document).ready(function(){
  var target = $('.related-products .teaser7');
  var carousel_Inner = target.find("ul.thumbnails");
  var child_Element = target.find("ul.thumbnails li");
  var child_Count = child_Element.length;
  var indicators;
  var count=0;
  
  
  target.addClass("carousel slide hero_carousel_2");
  target.attr('id', 'product-carousel');;
  carousel_Inner.addClass("carousel-inner");
  child_Element.each(function(){
  if(count==0){$(this).addClass("active")};
  $(this).addClass("item");
  count++;
  });
  for(i=0;i<child_Count;i++){
      if(i==0){
      indicators = "<ol class='carousel-indicators'><li data-target='#product-carousel' data-slide-to='"+i+"' class='active' ></li>";}
      else{indicators+= "<li data-target='#product-carousel' data-slide-to='"+i+"' ></li>";}
  };
  target.append(indicators);
  $('.carousel').carousel();
         
     /* Swipe event to slide the carousel*/
          $carouselSel=$("#product-carousel");
          $carouselSel.on('swipeleft', function (e) {
              e.stopPropagation();
              $carouselSel.carousel('next');
            }).on('swiperight', function (e) {
              e.stopPropagation();
              $carouselSel.carousel('prev');
            }).on('movestart', function (e) {
              if ((e.distX > e.distY && e.distX < -e.distY) || (e.distX < e.distY && e.distX > -e.distY)) {
                e.preventDefault();
                return;
              }
       });
          
  });
  }
     
/** End Related Products mobile carousle**/    
    
/**article page video player JS**/

    $(document).ready(function(){
    $('.video').parent().click(function () {
    if($(this).children(".video").get(0).paused){
        $(this).children(".video").get(0).play();
        $(this).children(".playpause").fadeOut();
    }else{
        $(this).children(".video").get(0).pause();
        $(this).children(".playpause").fadeIn();
    }
    });
    });
    
/**  END Article page video **/

/*PDP ACCORDION*/ 
if($("body").hasClass("single-products")){ 
     
   var headers = $('#accordion .accordion-header ');
   var accordianBtn=$('#accordion .accordion-link ');
   var accordianContent=$('#accordion .accordion-dropdown ').hide();
    headers.click(function() {
    var panel = $(this).next().find('.accordion-dropdown');
    var isOpen = panel.is(':visible');
    
    /* open or close as necessary*/
     if(isOpen){
       panel.slideUp();
      
     }else{
        $('#accordion .accordion-dropdown ').slideUp().hide()
         panel.slideDown();
        $('html, body').animate({
        scrollTop: $(this).offset().top-$('header').height()
         }, 1000);
        
      }
        
        
   
    /* stop the link from causing a pagescroll*/
    return false;
    });
}
    
    $(document).ready(function(){
        var PDP = $(document).find('.single-products');
        if(PDP.length > 0){
            $('.accordion-menu').each(function(){
                if($(this).text().length < 1){
                    $(this).parent().hide();
                }
            });
        }
    });
/*END PDP ACCORDION*/ 

    
/*Smooth scroll*/
    $(function() {
      $('a.accordion-menu[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^//'') == this.pathname.replace(/^//'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top-$('header').height()
            }, 1000);
            return false;
          }
        }
      });
    });
/*End Smooth scroll*/ 
   
         /*PLP Filter Module*/
var globalDataStructure = '';
var plpProductsObj = "";
var filterObj = "";

    window.onload = function() {

        $('<div id="LoadingImage" style="display: none"></div>').appendTo($('#main'));

        if($('body').is('#product')){
        /*Adding dynamic id's to tilesContent*/
        $('#product .tilesContent .productList').each(function(i) {
            $(this).attr('id','group_'+i);
            globalDataStructure += this.outerHTML;
        });
        plpProductsObj = $('.product-carousel').html();
        filterObj = jQuery(".product-carousel .tilesContent:first .pullRight");
        }
    };
    
    $(window).on("orientationchange onresize",function(){
         
       
            
          $('#quickViewContainer').remove();
        
          setTimeout(function (){
              $('#product .product-carousel li.active').find('.quickView').click();
          },200);
           
       
        
    });

  var createFrame = function (srcAppend, width, height, src) {
          var tmpSrc = src;
          if (tmpSrc.indexOf('?') === -1)
          {
              tmpSrc = tmpSrc + '?wmode=transparent';
          }
          $('<iframe />', {
              frameborder: '0',
              allowTransparency: 'true',
              width: width,
              height: height,
              //src: src+'?wmode=transparent'
              src: tmpSrc
          }).appendTo(srcAppend);

      };
    filterObj = $(".product-carousel .tilesContent:first .pullRight");
    plpProductsObj = $('.product-carousel').html();
    $('#product').on('change', '.pullRight select', function () {
        //var $this = $(this),
        var selectedFilter = jQuery(this).val(),
                liProductList = '',
                productsliHTML = '',
                productTag = '',
                parentId = $(this).find('option:selected').data('parent'),
                filterslug = $(this).find('option:selected').data('slug');
        
        //console.log(filterslug);
    if (this.value.toLowerCase() == 'all') {
            $('.product-carousel').html(plpProductsObj);
        } else {
            var show_filter_count = 0;
      var show_filter = 0;
            $('.productList ul').each(function () {
                var $this = $(this);
                var counter = 0;
        $this.parents('.tilesContent').find('.pullRight').remove();
                $this.children().each(function () {
                    if ($(this).hasClass(filterslug)) {
                        counter++;
                        $(this).show();
            if (show_filter == 0) {
              $this.parents('.tilesContent').find('.categoryHeading').append("<div class='pullRight'>" + filterObj.html() + "</div>");
              $this.parents('.tilesContent').find('select').val(selectedFilter);
              show_filter++;
              //console.log(show_filter);
            }
                    } else {
                        $(this).hide();
                    }
                });
                show_filter_count = parseInt(show_filter_count) + parseInt($this.find("." + filterslug).length);
                if (counter == 0) {
                    $this.parents('.tilesContent').hide();

                } else {
                    $this.parents('.tilesContent').show();
                }
        
                
            });
  
            if (show_filter_count > 1) {
                productTag = show_filter_count + ' PRODUCTS';
            } else {
                productTag = show_filter_count + ' PRODUCT';
            }
            $('.product-carousel').find('.prod_count').text('(' + productTag + ')');
        }

    });

        $(document).on('click' , '#product .quickView', function(){
            if($('#product').find('#quickViewContainer').length > 0){
                $('#product').find('#quickViewContainer').remove();
            }
            var $this = $(this),
                quickViewDiv = '';
            var currentLi = $this.closest('li'),
               currentLiPosition = currentLi.offset().top;
               $('#product .product-carousel li').removeClass('active');
              currentLi.addClass('active');
            var nextLi = currentLi.next('li');
            while (nextLi.length != 0 && currentLiPosition == nextLi.offset().top) {
                nextLi = nextLi.next('li');
            }
            


            if(prodjson.data.ProductDetail.length > 0){
                $(prodjson.data.ProductDetail).each(function(i) {
                    if($this.closest('li').attr('data-id') == prodjson.data.ProductDetail[i].id){
                        quickViewDiv = fillQuickViewData(prodjson.data.ProductDetail[i]);
                        $('.arrow-up').css('display','none');
                        $this.closest('li').find('.arrow-up').css('display','block');
                        if(nextLi.length != 0){
                            nextLi.before(quickViewDiv);
                        } else {
                              $this.closest('ul').append(quickViewDiv);  
                        }
                        if(whichDevice() != 'desktop'){
                        $('#product #quickViewContainer .buy-it-now-btn').attr('data-target','');
                        }
                        $('html, body').animate({
                        scrollTop: $($this).offset().top
                    }, 800);
                        return false;

                    }
                });
            }
        });

        var fillQuickViewData = function($currentObj){

            var findAStore = $('#store-locator-form'),
                    storeDataObj = $currentObj.quick_view.store_data,
                    selectProductSize='',
                    findAStoreBtnHTML='',
                    findAStore_href = findAStore.attr('action');

            if($currentObj.quick_view.find_store){

                findAStore.find('#where_to_buy_zipcode').val('');
                findAStore.find('select.miles option:first').attr("selected", true);
                findAStore.find('input.zipcode-input').removeClass('errorInput');
                findAStore.find('select.miles').removeClass('errorInput');
                findAStore.find('span.error').remove();

                findAStore.find('#ctname').val(storeDataObj.ctname);
                findAStore.find('#pid').val($currentObj.id);
                findAStore.find('#pname').val($currentObj.title);
                findAStore.find('#upc_10').val(storeDataObj.upc_10);
                findAStore.find('#upc_12').val(storeDataObj.upc_12);

                for(var i=0;i<storeDataObj.product_size.length;i++){

                    selectProductSize += '<option value="'+storeDataObj.product_size[i].size+'" data-cname="'+$currentObj.title+'" data-upc_10="'+storeDataObj.product_size[i].upc+'" data-upc_12="'+storeDataObj.product_size[i].upc_12+'">'+storeDataObj.product_size[i].size+'</option>';
                }
                findAStore.find('#prod_ddl').html('');
                findAStore.find('#prod_ddl').append(selectProductSize);

                findAStoreBtnHTML = '<a href="'+findAStore_href+'/#'+$currentObj.id+'" role="button" id="productStoreLocator" class="btn store-locator" title="'+$currentObj.title+'" >Find a store</a>';
            }

             

            var quickViewerHTML = '<div id="quickViewContainer">'+              
            '<section class="imgViewer">'+
            '<a href="'+$currentObj.href+'">'+
            '<figure style="background-image:url('+$currentObj.quick_view.image.backgroud_url+')">'+
                '<img class="lazy" src="'+$currentObj.quick_view.image.src+'" data-mob="" alt="'+$currentObj.quick_view.image.alt+'" title="'+$currentObj.quick_view.image.title+'" style="display: block;">'+
            '</figure>'+
            '</a>'+
            '</section>'+
            '<section class="contentViewer">'+
            '<a class="closeButton" id="closeProductDetail">'+$currentObj.quick_view.close_text+'</a>'+
            '<h2>'+$currentObj.title+'</h2>'+
            '<p>'+$currentObj.content+'</p>'+
            '<a href="'+$currentObj.href+'">'+$currentObj.quick_view.cta_text+'</a><br>'+
            '<p>'+$currentObj.quick_view.product_size+'</p>';
            if($currentObj.quick_view.add_to_cart !=null){
				quickViewerHTML = quickViewerHTML + $currentObj.quick_view.add_to_cart;
			}
            quickViewerHTML = quickViewerHTML + findAStoreBtnHTML +
            '</section>'+ 
            '</div>';
            
          
          //buyNowContainer.find('iframe').attr('src', $currentObj.quick_view.buy_now.api);
          //UDM.evq.push(['trackEvent', 'Custom', 'Product Quick View', ''+ $currentObj.title +'']);
            return quickViewerHTML;
        };     

         $("#buy_it_now").on("shown", function () {

                      var height = parseInt($(".modal-body").height()),
                            width = parseInt($(".modal-body").width()),
                            tempSrc = $('#product #buy_it_now').attr('data-iframeSrc');

                    /*//First check if the page is from store locator
                    if ($('body#store-locator').length > 0 || $('body#where_to_buy').length > 0)
                    {
                        tempSrc = $(".buy-it-now-btn").attr("data-url");
                    }
                    $(this).css({"width": width, "height": height});

                    $(".modal-body", this).css({"max-width": width, "max-height": height});*/

                    //Create the Iframe with given source and height/width from user
                    createFrame("#buy_it_now .modal-body", (width + 16), (height + 16), tempSrc);

                });
      
       $("#buy_it_now").on("hidden", function () {
                    $('.modal-body iframe').remove();
                });


        $(document).on('click' , '#product #closeProductDetail', function(){

            $('#quickViewContainer').remove();
            $('#product .product-carousel li.active').removeClass('active');
            $('.arrow-up, #product #store-locator-form').css('display','none');

        });
        $('.close-store-form').click(function(){
            $(this).closest('form').hide();
        });
        $(document).on('click' , '#product #productStoreLocator', function(e){

            var findAStore = $('#store-locator-form');
            if (whichDevice() != 'desktop') {

            }
            else { 
                findAStore.css({
                'display': 'block',
                'position': 'absolute',
                'left': $(this).offset().left,
                'top': $(this).offset().top + $(this).height() + 30 - $('header').height()
                }).show("slow");
                e.preventDefault();
            }
        });

/*        $('#store-locator-form #prod_ddl').on('change', function() {
            var $this = $(this),
                findAStore = $('#store-locator-form');
            findAStore.find('#upc_10').val($this.find(':selected').attr('data-upc_10'));
            findAStore.find('#upc_12').val($this.find(':selected').attr('data-upc_12'));
            findAStore.find('#ctname').val($this.find(':selected').attr('data-cname') +' '+ $this.find(':selected').val());
        });*/

        $('#product').click(function(e) {
            var container = $("#quickViewContainer");
                find_store = $('#store-locator-form');
                buyItNowPopup = $('#buy_it_now'),
                modalFadeInOverlay = $('.modal-backdrop');

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0 && !find_store.is(e.target) && find_store.has(e.target).length === 0
                && !buyItNowPopup.is(e.target) && buyItNowPopup.has(e.target).length === 0
                && !modalFadeInOverlay.is(e.target) && modalFadeInOverlay.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.remove();
                $('#product .product-carousel li.active').removeClass('active');
                $('.arrow-up').css('display','none');
                find_store.css('display','none');

            }
        });

/*Article Page Js Starts*/

            //var ajaxurl = 'http://stives-redesign-dev.unileversolutions.com/wp-admin/admin-ajax.php';
            var    ajaxSecurityToken = '';


        var ajax = function(options) {

             $(document).find("#LoadingImage").show();

            var url = options.url,
                data = options.data || {},
                dataType = options.dataType || 'json',
                success = options.success || function () {},
                error = options.error || function () {},
                //type = options.type || 'GET',
                type = options.type || "POST",
                requestType = options.requestType || true;

            var xhr = $.ajax({
                url: url,
                type: type,
                data: data,
                dataType: dataType,
                async: requestType,
                success : function (response) {
                     $(document).find("#LoadingImage").hide();
                    success(response);
                },
                error: function (jqXHR, exception) {
                     $(document).find("#LoadingImage").hide();
                    error(jqXHR, exception);
                }
            });
            return xhr;
        };


$( document ).ready(function() {

      if($('body').is('.article-landing')){
            var options = {
                    'url' : ajaxurl,
                    'data' : { 
                        'action' : 'get_nonce_key'
                    },
                    'success' : successCallback_ForTokenKey,
                    'error' : errorCallback_ForTokenKey
                }; 

            var ajaxObj = ajax(options);   
      }

      
});


var successCallback_ForTokenKey = function(response) {

        ajaxSecurityToken = response.nonce_val;
} ;

var errorCallback_ForTokenKey = function(jqXHR, exception) {
    console.log("Got Some Error From Server: "+jqXHR);

};

var successCallback_ArticleData = function(response) {

    var data = response.data,
        articleliHTML = '',
        className = '',
        productTag = '';
    if(data != undefined ) {  
   for(var i=0;i<data.length;i++){

    if(i % 2 == 0){
        className = 'image-right';
    }else{
        className = '';
    }

   articleliHTML +=    '<li class='+className+'>'+
                        '<section class="articleImgContent">'+
                         '<figure class="imageContainer">'+
                            '<img alt="'+data[i].image.alt+'" src="'+data[i].image.src+'" title="'+data[i].image.title+'">'+
                         '</figure>'+
                      '</section>'+
                      '<section class="articleTextContent">'+
                         '<div class="table-block">'+
                            '<div class="cell-block">'+
                            '<h3>'+data[i].title+'</h3>'+
                            '<p>'+data[i].content+'</p>'+
                            '<a href="'+data[i].href+'" '+ data[i].cta_tags + 'class="btn btn-small cta_frombean" title="">'+data[i].btn_text+'</a>'+
                         '</div>'+
                         '</div>'+
                      '</section>'+
                    '</li>';

} 
}

 
if($(document).find('#article .load-more').hasClass('clicked')){
    $(document).find('#article .articleListArea > ul').append(articleliHTML);
    $(document).find('#article .load-more').removeClass('clicked');
    jQuery(CT.track.init);
}else{
    $(document).find('#article .articleListArea > ul').html('')
                                                  .html(articleliHTML); 
}
if(response.loadMore == false){  
    $(document).find('#article .load-more').hide();
}else{     
    $(document).find('#article .load-more').show();
}

    if(response.data_count > 1){
            productTag = response.data_count +' ARTICLES';
        }else{
            productTag = response.data_count +' ARTICLE';
        }
        $(document).find('#article .prod_count').text('('+productTag+')');
};

var errorCallback_ArticleData = function(jqXHR, exception) {
    console.log("Got Some Error From Server: "+jqXHR);

};
    
$(document).on('keypress', '#signup-form  #recaptcha_response_field, #contactform  #recaptcha_response_field', function(e){
  var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
  if(keyCode===13){
    e.preventDefault();
    jQuery('#signup-form, #contactform').find('input[type="submit"]').click();

  }

});

$(document).on('submit', '#signup-form, #contactform', function(e){
  e.preventDefault();
});     

$('#article #category, #article #article_filter ').on('change', function() {

  var $this = $(this),
      selectedFilterType = $this.find(':selected').attr('data-type') || '',
      selectedFilterValue = $this.find(':selected').attr('data-category') || '';

      if(selectedFilterValue != ''){
     var selectedFilter = selectedFilterType +':'+ selectedFilterValue;
      }

   var otherFilter = $this.siblings('select'),
        otherFilterType = otherFilter.find(':selected').attr('data-type') || '',
        otherFilterValue = otherFilter.find(':selected').attr('data-category') || '';

    if(otherFilterValue != ''){
     var otherSelectedFilter = otherFilterType +':'+ otherFilterValue;
      }
    var selectedFilterValues = $.grep([selectedFilter, otherSelectedFilter], Boolean).join(", ");
   



  var options = {
                    'url' : ajaxurl, 
                    //'url' : 'data/grid.json',
                    'data' : {
                        'security' : ajaxSecurityToken,
                        'action' : 'get_filter_articles',
                        //'id' : $(globalButton).attr('data-tax-id'),
                        'offSet':'0',
                        'filter': selectedFilterValues
 
             
                    },
                    'success' : successCallback_ArticleData,
                    'error' : errorCallback_ArticleData
                }; 

    if(ajaxSecurityToken != ''){
        ajax(options);
    }
     
});

$(document).on('click' , '#article .load-more', function(){

var selectFilterDropdown = $(this).parent(),
    categoryFilter = '',
    articleFilter = '',
    selectedFilterValues = '',
    offsetvalue = '';

$(this).addClass('clicked');

var categoryFilterType = selectFilterDropdown.find('#category :selected').attr('data-type') || '',
    categoryFilterValue =  selectFilterDropdown.find('#category :selected').attr('data-category')|| '';

var articleFilterType = selectFilterDropdown.find('#article_filter :selected').attr('data-type')|| '',
    articleFilterValue =  selectFilterDropdown.find('#article_filter :selected').attr('data-category')|| '';
    

    if(categoryFilterValue != ''){
         categoryFilter = categoryFilterType +':'+ categoryFilterValue;
    }

    if(articleFilterValue != ''){
         articleFilter = articleFilterType +':'+ articleFilterValue;
    }

    selectedFilterValues = $.grep([categoryFilter, articleFilter], Boolean).join(", ");
    offsetvalue = $(this).parent().find('li').length;

    var options = {
                    'url' : ajaxurl, 
                    //'url' : 'data/grid.json',
                    'data' : {
                        'security' : ajaxSecurityToken,
                        'action' : 'get_filter_articles',
                        //'id' : $(globalButton).attr('data-tax-id'),
                        'offSet':offsetvalue,
                        'filter': selectedFilterValues
 
             
                    },
                    'success' : successCallback_ArticleData,
                    'error' : errorCallback_ArticleData
                }; 
    if(ajaxSecurityToken != ''){
    ajax(options); 
    }

});
    
 
    
   /** country selector js **/
    
    $(document).on('click' ,'#selector', function(){
      $("#footerInitial").css({'opacity':'0','z-index':'-1'});
      $("#displaySelector").fadeIn();
        if(window.innerWidth<767){
            $('html, body').animate({
                scrollTop: $('#footer').offset().top
                 }, 1000);
        }
      });
    $(document).on('click' ,'.closeFooter', function(){
      $("#displaySelector").fadeOut();
      $("#footerInitial").css({'opacity':'1','z-index':'1'});
      
      
      });
    
    $(document).ready(function(){
        $("#displaySelector").css('display','none');
    });
 

    
    var i=0;
    var n= Object.keys(country_selector_json).length;
for (var key in country_selector_json) {

    if (country_selector_json.hasOwnProperty(key)) {
          var div = document.getElementById('countryValues');
          var list = document.createElement("ul");
          list.id=key;   
          //var mq = window.matchMedia( "(min-width: 1024px)" );
          //if(mq.matches){
            if($(window).width()  >= 1024){
            var p = (100/n)-2;
            list.style.width= p+"%";
           }
          else{
              $('#countryValues').css('display','block');
              $('#countryValues').css('text-align','center');
              $('#countryValues').css('padding-left','0px');
              list.style.width= '100'+"%";
            }
        
          if(i!=0){
            list.className = 'category-footer';
          }
          
          var heading = document.createElement("h4");
          var region = country_selector_json[key];
          heading.innerHTML = key;
          list.appendChild(heading);
        
        for (var key in region) {
          div.appendChild(list);
          var country = region[key];
            if (region.hasOwnProperty(key)) {

              if(country.is_current_site === 1){
                    var curr = country.label;
                }
              else{
                var aTag = document.createElement('a');
                aTag.setAttribute('href',country.url);
                aTag.innerHTML = country.label;
                var li = document.createElement("li");
                li.appendChild( aTag );
                list.appendChild(li);
              }
            }
          }
        i++;
      }   
        var div = document.getElementById('selector');
        div.innerHTML = curr;
    
        var div = document.getElementById('selectorFinal');
        div.innerHTML = curr;   
   
  }
    /** Country Selector JS ends here **/
    
    /** Site Map JS begins **/
    
    var x =document.getElementsByClassName('menu-item-object-product-category');
    var y =document.getElementsByClassName('menu-item-object-custom menu-item-has-children');
  

    for(var i=0;i< x.length; i++){
        var hrtag = document.createElement("hr");
        hrtag.className = "site-map-hr";
        x[i].parentNode.insertBefore(hrtag, x[i]);
    }
    
    for(var i=0;i< y.length; i++){
        var hrtag = document.createElement("hr");
        hrtag.className = "site-map-hr";
        y[i].parentNode.insertBefore(hrtag, y[i]);
    }
    
    
    /** Site Map JS Ends **/
	
      
    /** Dynamic Olapic gallery Code **/ 
      jQuery('.olapic-gallery-page').append('<script type="text/javascript" src="https://photorankstatics-a.akamaihd.net/81b03e40475846d5883661ff57b34ece/static/frontend/latest/build.min.js" data-olapic="olapic_specific_widget" data-instance="929648a55b67c95f92bd4774c89a96bd" data-apikey="e96e3a4708b9db6f4103b704a49cf5adcd2aca369acc12f6c176b0f1ab7cf3b6" async="async"></script>');
	
	 /** location icon**/
	  jQuery(jQuery(".menu-header-navigation-container .storelocationicon").detach()).appendTo(".menuWrapper");

     

})(jQuery);
 
 
/**timestamp1516086668**/